
Device Onboarding script
========================

This script exposes a basic device on boarding process. It installs the required components and sequentially asks the user how much to system resource to allocate to NuNet and also the users payment address. 

System requirement for the onboarding script to function as needed
------------------------------------------------------------------

we only require for you to specify CPU and RAM but your system must have the following requirements before you decide to onboard it.


* CPU - 2000 mhz
* RAM    - 4 GB
* DISK SPACE  - 10 GB

To install the required packages ``sudo`` access is required.

Via direct script download
--------------------------

Onboarding
^^^^^^^^^^

The following commands clone the repo, install and setup your device for the onboarding process.

``wget https://gitlab.com/nunet/nunet-infra/-/raw/master/device_onboarding/nunet-onboarding.sh``

``bash nunet-onboarding.sh setup``

Leaving the platform
^^^^^^^^^^^^^^^^^^^^

``bash nunet-onboarding.sh remove``

Via manual clone of the repository
----------------------------------

You can also download the full ``nunet-infra`` repository which contains device onboarding script.


#. 
   clone this repository to your machine

    ``git clone https://gitlab.com/nunet/nunet-infra``


#. 
   go to the device onbording directory

    ``cd nunet-infra/device_onboarding``

#. 
   download and install nomad, docker, python, podman and the requirements to run the python script.

    ``bash onboarding.sh install``   

#. 
   generate device metadata and nomad client configuration.Then it install nomad configuration on the system. This option can also be used to update allocated compute resources and payment identity after first initialization.

    ``bash onboarding.sh config``

To do all the process at once
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

   ```bash onboarding.sh all```


To remove your device
^^^^^^^^^^^^^^^^^^^^^

.. code-block::

   ```bash onboarding.sh remove```
